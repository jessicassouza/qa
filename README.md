# QA Automation - Moises Studio

This repository contains automated end-to-end tests using Cypress for the studio.moises.ai platform. It ensures key functionalities such as account creation, song upload, and track separation perform as expected in the production environment.

## Installation

Clone the repository and install dependencies:

```
git clone https://github.com/jessicaschelly/moises-qa
cd moises-qa
yarn
```


### How to Run

To execute tests:

`yarn cy:run` 

To open the Cypress interface and run tests interactively:

`yarn cy:open` 

### Technical Choices

The choice of Cypress as testing framework was driven primarily by its widespread adoption and community support. Cypress offers a rich set of features for browser-based end-to-end testing, making it ideal for simulating real user interactions on studio.moises.ai. 

### Setup Instructions

To run the tests locally, certain environment variables need to be set. While CI/CD environments are pre-configured with the credentials, local testing requires manually setting the user password:

The command below includes the user password directly in the run command, ensuring environment-specific configurations are met.

`USER_PASSWORD=password yarn cy:run` 

Or you could also add on an .env file



### Known Issues and Limitations

1.  **Account Cleanup:** Each test run currently creates new user accounts, which are not deleted post-testing. Future improvements could include automating account cleanup to prevent database clutter.
    
2.  **Upload Verification:** Verifying the successful upload and separation of music files is currently challenging. Further analysis is needed to enhance the reliability of upload checks, potentially through deeper integration with backend services to confirm file processing status.
3. **UI Changes Impact:** Tests currently are sensitive to UI changes, and frequent updates could lead to the test being outdated. 

## Current state of automated tests:
- Account Creation with a Random Email ✔️ - "Faker" library was used to ensure email uniqueness for each test run
- Upload a Song ✔️
- Track Separation ✔️
- Validate Request Input Value ✔️
- Hiding the Moises Collection ✔️
- Deleting the Uploaded Song ✔️
- Retry ✔️