// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })


Cypress.Commands.add('login', (email, password) => {
  import('../pages/authPage').then((module) => {
    const authPage = module.default
    cy.session([email, password], () => {
      authPage.visit()

      authPage.emailInput.type(email)
      authPage.passwordInput.type(password)
      authPage.loginButton.click()

      cy.contains('Track Separation').should('be.visible')
    })
  })
})

Cypress.Commands.add('signup', (email, password) => {
  import('../pages/authPage').then((module) => {
    const authPage = module.default
    cy.session([email, password], () => {
      authPage.visit()

      authPage.navigateSignUpButton.click()
      authPage.emailInput.type(email)
      authPage.passwordInput.type(password)
      authPage.signupButton.click()

      cy.contains('Track Separation').should('be.visible')
    })
  })
})

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to select DOM element by data-cy attribute.
     * @example cy.dataCy('greeting')
     */
    login(email: string, password: string): Chainable<JQuery<HTMLElement>>

    signup(email: string, password: string): Chainable<JQuery<HTMLElement>>
  }
}
