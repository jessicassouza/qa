class AuthPage {
  visit() {
    return cy.visit('https://studio.moises.ai/login')
  }

  get navigateSignUpButton() {
    return cy.get('#sign_up_button')
  }

  get emailInput() {
    return cy.get('#email_address_textbox')
  }

  get passwordInput() {
    return cy.get('#password_textbox')
  }

  get signupButton() {
    return cy.get('#signup_button')
  }

  get loginButton() {
    return cy.get('#login_button')
  }

  get emailError() {
    return cy.contains('Please enter a valid email address')
  }

  get passwordError() {
    return cy.contains('At least 6 characters long')
  }

  get existingEmailError() {
    return cy.contains('The email address is already in use by another account.')
  }
}

export default new AuthPage()
