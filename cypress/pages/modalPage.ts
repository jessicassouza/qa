class ModalPage {
  get modalButtonConfirm() {
    return cy.get('#modal_button_confirm')
  }
}

export default new ModalPage()
