class SongPage {

  get actionsTaskButton() {
    return cy.get('button[class*="actions-task_button"]')
  }

  get deleteFromLibraryOption() {
    return cy.get('#library_song_delete_from_library_option')
  }

  get changeOperationsButton() {
    return cy.get('div[class*="change-operations_container"] > button')
  }
}

export default new SongPage()
