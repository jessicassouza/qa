class LibraryPage {
  visit() {
    return cy.visit('https://studio.moises.ai/library')
  }

  get addNewSongButton() {
    return cy.get('#add_new_song_button')
  }

  get fileInput() {
    return cy.get('input[type="file"]')
  }

  get uploadNextButton() {
    return cy.get('#upload_next_button')
  }

  get uploadSubmitButton() {
    return cy.get('#upload_submit_button')
  }

  get vocalsAccompanimentButton() {
    return cy.get('#vocals-accompaniment')
  }

  get title() {
    return cy.contains('Track Separation')
  }

  musicButton(name: string) {
    return cy.get('button').contains(name)
  }

  get playlistHideButton() {
    return cy.get('#playlist_hide_button')
  }

  get moisesPlaylistTitle() {
    return cy.contains('Discover our demonstration setlist Moises Collection')
  }

}

export default new LibraryPage()
