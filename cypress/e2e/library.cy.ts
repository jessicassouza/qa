import { faker } from '@faker-js/faker';
import libraryPage from '../pages/libraryPage';
import songPage from '../pages/songPage';
import modalPage from '../pages/modalPage';
import { aliasMutation } from '../support/graphqlUtils';

const song = 'cypress/fixtures/files/TESTE_FAQ.mp3'
const email = faker.internet.email({ provider: 'qatest.com' })
const password = Cypress.env('USER_PASSWORD')

describe('Library actions', () => {
  before(() => {
    cy.signup(email, password)

    cy.intercept('POST', '/graphql', (req) => {
      aliasMutation(req, 'createTask')
    })
  })

  it('should upload a song succesfully', () => {
    libraryPage.visit()
    libraryPage.addNewSongButton.should('be.visible').click()

    libraryPage.fileInput.selectFile(song, { force: true })
    cy.contains('TESTE_FAQ.mp3').should('be.visible')

    cy.intercept('/v3/upload').as('upload')
    cy.wait('@upload').its('response.statusCode').should('eq', 200)

    libraryPage.uploadNextButton.click()
    libraryPage.vocalsAccompanimentButton.should('contain.text', '2 Tracks').click()

    libraryPage.uploadSubmitButton.click()

    cy.wait('@gqlcreateTaskMutation').then((response) => {
      expect(response.response.statusCode).to.eq(200)
      expect(response.request.body.query).to.contain('input: "TESTE_FAQ.mp3"')
    })

    cy.contains('Uploading...').should('be.visible')
    cy.contains('Uploading...').should('not.exist')

    libraryPage.title.should('be.visible')
    libraryPage.musicButton('TESTE_FAQ').should('be.visible').click()

    songPage.changeOperationsButton.contains('2 Tracks').should('be.visible')
  })

  it('should delete the uploaded song', () => {
    libraryPage.visit()
    libraryPage.musicButton('TESTE_FAQ').should('be.visible').click()
    songPage.actionsTaskButton.click()
    songPage.deleteFromLibraryOption.click()
    modalPage.modalButtonConfirm.click()

    cy.contains('TESTE_FAQ').should('not.exist')
    cy.contains('Start adding songs').should('be.visible')
  })

  it('should hide the moises collection', () => {
    libraryPage.visit()

    libraryPage.moisesPlaylistTitle.should('be.visible')
    libraryPage.playlistHideButton.click()
    modalPage.modalButtonConfirm.click()
    libraryPage.moisesPlaylistTitle.should('not.exist')
  })
})
