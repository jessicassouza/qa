import { faker } from '@faker-js/faker';
import authPage from '../pages/authPage';
import libraryPage from '../pages/libraryPage';

const email = faker.internet.email({ provider: 'qatest.com' })
const password = Cypress.env('USER_PASSWORD')

describe('Account Creation', () => {
  it('should not create a new account with an invalid email', () => {
    authPage.visit()

    authPage.navigateSignUpButton.should('be.visible').click()
    authPage.emailInput.type('test')
    authPage.passwordInput.type(password)

    authPage.signupButton.click()
    authPage.emailError.should('be.visible')
  })

  it('should not create a new account with an invalid password', () => {
    authPage.visit()

    authPage.navigateSignUpButton.should('be.visible').click()
    authPage.emailInput.type(email)
    authPage.passwordInput.type('123')

    authPage.signupButton.click()
    authPage.passwordError.should('be.visible')
  })

  it('should create a new account succesfully', () => {
    authPage.visit()

    authPage.navigateSignUpButton.should('be.visible').click()
    authPage.emailInput.type(email)
    authPage.passwordInput.type(password)

    authPage.signupButton.click()

    libraryPage.title.should('be.visible')
  })
})

