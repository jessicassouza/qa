require('dotenv').config()

const { defineConfig } = require("cypress");

module.exports = defineConfig({
  retries: 2,
  env: process.env,
  defaultCommandTimeout: 20000,
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
